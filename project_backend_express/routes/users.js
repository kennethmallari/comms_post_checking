var createError = require("http-errors");
var express = require("express");
var router = express.Router();
const bcrypt = require("bcrypt");
const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";

const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("users");

// GET ALL Users
router.get("/", function (req, res, next) {
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// GET Single User:
router.get("/:id", function (req, res, next) {
  collection.findOne(
    {
      _id: new ObjectId(req.params.id),
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//Get User Info by userId:
router.get("/api/:id", function (req, res, next) {
  collection.findOne(
    {
      userId: req.params.id,
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

router.post("/", function (req, res, next) {
  collection.insertOne(
    {
      userId: req.body.userId,
      fullName: req.body.fullName,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 10),
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//Updating a User
router.put("/:id", function (req, res, next) {
  collection
    .findOneAndUpdate(
      { _id: new ObjectId(req.params.id) },
      {
        $set: {
          fullName: req.body.fullName,
          email: req.body.email,
        },
      },
      {
        upsert: true,
      }
    )
    .then((result) => res.json(result));
});

// Delete single user
router.delete("/:id", function (req, res, next) {
  collection.deleteOne(
    {
      _id: new ObjectId(req.params.id),
    },
    function (err, result) {
      if (err) throw err;
      res.send(result);
    }
  );
});

router.post("/auth", function (req, res, next) {
  collection.findOne(
    {
      email: req.body.email,
    },
    function (err, result) {
      console.log("result of the condition", result === null);
      if (result === null) {
        res.send(false);
      } else {
        let comparePasswordResult = bcrypt.compareSync(
          req.body.password,
          result.password
        );
        if (comparePasswordResult) {
          req.session.user = result;
          console.log(req.session.user);
          console.log("this is the result: ", result);
          res.json(result);
        }
      }
    }
  );
});

module.exports = router;
