var express = require("express");
var router = express.Router();
var http = require("http");
var formidable = require("formidable");
var fs = require("fs");
var path = require("path");

const { MongoClient, ObjectId } = require("mongodb");

var url = "mongodb://localhost:27017/";
const client = new MongoClient(url);
client.connect();
var dbo = client.db("project_dashboard");
const collection = dbo.collection("uploads");

//GET all uploads:
router.get("/", function (req, res, next) {
  collection.find({}).toArray(function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

//GET specific upload using userId:
router.get("/myUploads", function (req, res) {
  collection
    .find({
      userId: Number(req.query.userId),
    })
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      console.log(req.query.userId);
      console.log("data:", res.data);
    });
});

// GET Upload using uploadId:
router.get("/fetchSharedUsers", function (req, res) {
  collection.findOne(
    {
      uploadId: Number(req.query.uploadId),
    },
    function (err, result) {
      if (err) throw err;
      res.json(result);
    }
  );
});

//Adding a new upload:
router.post("/", function (req, res) {
  let form = new formidable.IncomingForm();

  form.parse(req, function (err, fields, files) {
    let parentFileName = Date.now() + "";
    let oldPath = files.fileUpload.filepath;
    let extension = path.extname(files.fileUpload.originalFilename);
    let newFileName = parentFileName + extension;
    let newPath = "./uploads/" + newFileName;

    fs.readFile(oldPath, function (err, data) {
      if (err) throw err;

      fs.writeFile(newPath, data, function (err) {
        if (err) throw err;
        collection.insertOne(
          {
            uploadId: parseInt(fields.uploadId),
            label: fields.fileDescription,
            userId: parseInt(fields.userId),
            fileName: files.fileUpload.originalFilename,
            parent: newFileName,
            sharedTo: [],
          },
          function (err, result) {
            if (err) throw err;
            res.json(result);
          }
        );
      });
    });
  });
});

//Updating an upload:
router.put("/", function (req, res, next) {
  console.log("upload id:", req.body.uploadId);
  console.log("type:", typeof req.body.uploadId);
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
      },
      {
        $set: {
          label: req.body.label,
        },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

//DELETING an upload
router.delete("/deleteUpload", function (req, res) {
  collection
    .deleteOne({
      uploadId: Number(req.body.uploadId),
    })
    .then((result) => {
      res.json(result);
    });
  //this will delete the file synchronously
  fs.unlinkSync("./uploads/" + req.body.fileName);
});

//Unshare File to user
router.put("/unshare", function (req, res) {
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
      },
      {
        $pull: { sharedTo: Number(req.body.userId) },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

// Share a upload to a user:
router.put("/share", function (req, res) {
  collection
    .findOneAndUpdate(
      {
        uploadId: Number(req.body.uploadId),
        sharedTo: { $ne: Number(req.body.userId) }, // { $not: { $elemMatch: [Number(req.body.userId)] } },
      },
      {
        $push: { sharedTo: parseInt(req.body.userId) },
      },
      {
        upsert: false,
      }
    )
    .then((result) => res.json(result));
});

// GET all shared uploads from a user */
router.get("/fetchSharedToUser", function (req, res) {
  collection
    .find({
      sharedTo: Number(req.query.userId),
    })
    .toArray(function (err, result) {
      if (err) throw err;
      res.json(result);
      console.log(req.query.userId);
      console.log("data:", res.data);
    });
});

module.exports = router;
