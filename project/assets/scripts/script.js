//Login Functionality with Ajax:
function validateLoginForm() {
  var emailVal = document.getElementById("email").value;
  var passwordVal = document.getElementById("password").value;

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  } else if (passwordVal == "") {
    alert("Please enter password");
    return false;
  } else if (passwordVal.length < 3) {
    alert("Password length must be atleast 3 characters");
    return false;
  } else {
    $.ajax({
      type: "POST",
      url: "http://localhost:3000/users/auth",
      data: { email: emailVal, password: passwordVal },
      success: function (response) {
        let sessionID = response.userId;
        if (response) {
          setLoginSession("user", sessionID);
          localStorage.setItem("loggedInUser", JSON.stringify(response));

          return true;
        } else {
          console.log(response);
          alert("Wrong Username/Password");
          return false;
        }
      },
      async: false, // <- this turns it into synchronous
    });
  }
}
function getLoginSession() {
  return sessionStorage.getItem("user");
}

function setLoginSession(key, value) {
  sessionStorage.setItem(key, value);
}

function deleteSession() {
  sessionStorage.removeItem("user");
}

function logOut() {
  deleteSession();
  localStorage.removeItem("loggedInUser");
  return true;
}

//Register Functionality with Ajax:
function validateRegistrationForm() {
  var fullName = document.getElementById("fullName").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var confirmPassword = document.getElementById("confirmPassword").value;

  var atPosition = email.indexOf("@");
  var dotPosition = email.lastIndexOf(".");

  if (fullName === "") {
    alert("Please enter your full name.");
    return false;
  } else if (email === "") {
    alert("Please enter your email.");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email.");
    return false;
  } else if (password === "") {
    alert("Please enter password.");
    return false;
  } else if (password.length < 3) {
    alert("Password length must be atleast 3 characters.");
    return false;
  } else if (confirmPassword === "") {
    alert("Please enter confirmation password.");
    return false;
  } else if (password !== confirmPassword) {
    alert("Password you entered didn't match.");
    return false;
  }

  var user = {
    userId: Date.now(),
    fullName: fullName,
    email: email,
    password: password,
  };

  $.ajax({
    type: "POST",
    url: "http://localhost:3000/users",
    data: user,
    success: function (response) {
      return true;
    },
    complete: function () {
      return true;
    },
  });
}

/*******  User's Read Update Delete: ********/

//Display users for UsersList Table dynamically using Ajax:

function diplayUsersAjax() {
  let data;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/users",
    success: function (response) {
      data = response;
      console.log(response);
    },
    async: false,
  });

  for (let i = 0; i < data.length; i++) {
    document.write(`
    <tr>
      <td class="primary-td">${data[i].fullName}</td>
      <td class= "td-center primary-td">${data[i].email}</td>
      <td class="td-center""><a href="./editUserPage.html?userid=${data[i]._id}">Edit</a> |
  <a
    href="#"
    class="delete-btn"
    data-bs-toggle="modal"
    data-bs-target="#exampleModal2"
    onclick="setFunction('${data[i]._id}')"
    >Delete</a
  ></td> 
    </tr>
    `);
  }
}

//For passing the ID to delete a user using AJAX:
var deleteUserID = 0;
function setFunction(id) {
  deleteUserID = id;
}

//Delete user using Ajax:
function deleteUser() {
  $.ajax({
    type: "DELETE",
    url: `http://localhost:3000/users/${deleteUserID}`,
    succes: function (response) {
      return true;
    },
    async: false,
  });
  location.href = "./usersList.html";
}

// For Populating the Edit User Page:

function populateEditPage() {
  const nameField = document.getElementById("full-name-field");
  const emailField = document.getElementById("email-field");
  let params = new URLSearchParams(window.location.search);
  let userId = params.get("userid");
  let data;

  console.log(nameField);

  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/${userId}`,
    success: function (response) {
      data = response;
      console.log(response);
    },
    async: false,
  });

  nameField.value = data.fullName;
  emailField.value = data.email;
}
//Updating the User Information:
function validateUpdateFormAjax() {
  var fullNameVal = document.getElementById("full-name-field").value;
  var emailVal = document.getElementById("email-field").value;
  let params = new URLSearchParams(window.location.search);
  let userId = params.get("userid");

  var atPosition = emailVal.indexOf("@");
  var dotPosition = emailVal.lastIndexOf(".");

  if (fullNameVal == "") {
    alert("Please enter full name");
    return false;
  } else if (emailVal == "") {
    alert("Please enter your email");
    return false;
  } else if (atPosition < 1 || dotPosition - atPosition < 2) {
    alert("Please enter valid email");
    return false;
  }
  let user = {
    fullName: fullNameVal,
    email: emailVal,
  };

  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/users/${userId}`,
    data: user,
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

//get users from DB:
function getUsers() {
  let users;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/users",
    success: function (response) {
      users = response;
      console.log(users);
    },
    async: false,
  });

  if (!users) {
    return [];
  } else {
    return users;
  }
}

//get the Loggedin user from the local storage:
function getUser() {
  const userRaw = localStorage.getItem("loggedInUser");
  const user = JSON.parse(userRaw);
  return user;
}

/******** Functions for Uploads:  ********/

//get the uploads from the DB:
function getUploads(userId) {
  let uploads = [];

  if (userId !== null) {
    $.ajax({
      type: "GET",
      url: "http://localhost:3000/uploads/myUploads",
      data: { userId: userId },
      success: function (response) {
        uploads = response;
        console.log(response);
      },
      async: false,
    });
  } else {
    $.ajax({
      type: "GET",
      url: "http://localhost:3000/uploads/allUploads",
      success: function (response) {
        uploads = response;
        console.log(response);
      },
      async: false,
    });
  }
  return uploads;
}

//Function for the Upload modal and in uploading a file:
function uploadModal() {
  const modal = document.querySelector("#uploadModal");
  const span = document.querySelector("#closeUpload");
  const form = document.querySelector("#uploadForm");
  const btnSave = document.querySelector("#save");
  const btnCancel = document.querySelector("#cancel");
  const inputFile = document.querySelector("#realButton");
  const btnChooseFile = document.querySelector("#chooseFile");
  const label = document.querySelector("#labelInput");
  const fileName = document.querySelector("#fileName");

  modal.style.display = "block";
  span.onclick = function () {
    modal.style.display = "none";
  };

  btnChooseFile.onclick = function () {
    modal.style.display = "block";
    inputFile.click();
  };

  inputFile.onchange = function () {
    if (inputFile.value) {
      modal.style.display = "block";
      fileName.innerHTML = inputFile.value.match(
        /[\/\\]([\w\d\s\.\-\(\)]+)$/
      )[1];
    }
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  btnSave.onclick = function () {
    if (inputFile.files.length !== 0 && label.value !== "") {
      let formData = new FormData(form);
      let sharedTo = [];
      formData.append("userId", getLoginSession("userId"));
      formData.append("uploadId", Date.now());
      formData.append("sharedTo", sharedTo);
      $.ajax({
        type: "POST",
        url: "http://localhost:3000/uploads",
        data: formData,
        dataType: "json",
        //for you to pass any data
        contentType: false,
        cache: false,
        //You will pass Raw data
        processData: false,
        complete: function (response) {
          window.location.reload();
        },
      });

      modal.style.display = "none";
    } else {
      alert("Please upload a file and add a label.");
    }
  };
}

//This function is to display the uploads table:
function displayUploadsTable() {
  const loggedInUser = getLoginSession();

  let uploads = getUploads(loggedInUser);
  const table = document
    .getElementById("uploadsTable")
    .getElementsByTagName("tbody")[0];

  if (uploads.length == 0) {
    table.innerHTML =
      "<tr><td class='alignCenter' colspan='3'>No Uploads...</td></tr>";
  } else {
    uploads.forEach((upload) => {
      let anchor = document.createElement("a");
      let row = table.insertRow();
      let cell1 = row.insertCell(0);
      let cell2 = row.insertCell(1);
      let cell3 = row.insertCell(2);
      let fileName = document.createTextNode(
        upload.fileName + "_" + upload.parent
      );
      anchor.appendChild(fileName);
      anchor.href = `./../../../project_backend_express/uploads/${upload.parent}`;
      let newLabel = document.createTextNode(upload.label);

      console.log(anchor.href);

      let actions = `<a class='noDeco' href='#' onclick='editUploadModal(${
        upload.uploadId
      }, "${upload.label}");'>Edit</a> |
       <a class='noDeco' href='#' onclick='deleteUpload(${upload.uploadId}, "${
        upload.fileName
      }"); '>Delete</a> |  
       <a class='noDeco' href='./sharedPage.html?uploadId=${
         upload.uploadId
       }&name=${upload.fileName + "_" + upload.parent}'>Share</a>`;

      console.log(actions);

      console.log(anchor);
      cell1.appendChild(newLabel);
      cell2.appendChild(anchor);
      cell3.innerHTML += actions;
      cell2.className = "alignCenter";
      cell3.className = "alignCenter";
    });
  }
}

//function for EditUploadModal:
function editUploadModal(uploadId, label) {
  console.log(label);
  const modal = document.querySelector("#editModal");
  const span = document.querySelector("#closeEdit");
  const btnUpdate = document.querySelector("#btnUpdate");
  const btnCancel = document.querySelector("#btn-edit-cancel");
  const newLabel = document.querySelector("#editLabelInput");
  modal.style.display = "block";

  newLabel.value = label;

  span.onclick = function () {
    modal.style.display = "none";
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  btnUpdate.onclick = function () {
    if (newLabel.value !== "") {
      modal.style.display = "none";
      const upload = { uploadId: uploadId, label: newLabel.value };
      $.ajax({
        type: "PUT",
        url: `http://localhost:3000/uploads/`,
        data: upload,
        success: function (response) {
          return true;
        },
        async: false,
        complete: function () {
          return true;
        },
      });
    } else {
      alert("Please add a label");
    }
  };
}

function deleteUpload(id, fileName) {
  const modal = document.querySelector("#deleteModal");
  const span = document.querySelector("#closeDelete");
  const btnOk = document.querySelector("#btnOk");
  modal.style.display = "block";

  span.onclick = function () {
    modal.style.display = "none";
  };
  btnOk.onclick = function () {
    modal.style.display = "none";
    $.ajax({
      type: "DELETE",
      url: `http://localhost:3000/uploads/deleteUpload`,
      data: { uploadId: id, fileName: fileName },
      succes: function (response) {
        return true;
      },
      async: false,
    });
    location.href = "./manageDocumentPage.html";
  };
}

//This function is to get the details of a specific user:
function getUserInfo(id) {
  let user;

  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/api/${id}`,
    success: function (data) {
      if (data) {
        user = data;
      }
    },
    async: false,
  });
  return user;
}

/**** For Shared Page: ****/
//For dropdown

function showRegisteredUsers() {
  let dropdown = document.querySelector("#usersDropdown");
  let btnSave = document.querySelector("#share");
  let users = getUsers();
  let sharedUsers = populateSharedUsers(uploadId);
  const activeUserId = getLoginSession();

  users = users
    .filter((y) => !sharedUsers.find((x) => x == y.userId))
    .map((y) => y);

  for (let user = 0; user < users.length; user++) {
    if (users[user].userId != activeUserId) {
      let option = document.createElement("option");
      option.value = users[user].userId;
      option.innerHTML = users[user].fullName;
      dropdown.appendChild(option);
    }
  }
  if (dropdown.options.length == 0) {
    btnSave.disabled = true;
  }
}

//This function is to get the Shared Users data:
function populateSharedUsers(uploadId) {
  let usersShared = [];

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/uploads/fetchSharedUsers",
    data: { uploadId: uploadId },
    success: function (response) {
      usersShared = response;
    },
    async: false,
  });
  return usersShared.sharedTo;
}

//This function is to display data in Shared Users Table:
function displaySharedUsersTable(id) {
  let sharedUsers = populateSharedUsers(id);

  for (user = 0; user < sharedUsers.length; user++) {
    var userId = sharedUsers[user];
    console.log(userId);
    var name = getUserInfo(userId);

    if (!name) {
      continue;
    }
    const table = document
      .querySelector("#sharedUserTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var newUser = document.createTextNode(name.fullName);

    var action = `<a class='noDeco' href='#' onclick='deleteSharedUserModal("${id}", " ${userId}"); return false;'>Remove</a>`;

    cell1.appendChild(newUser);
    cell2.innerHTML = action;
    cell2.className = "alignCenter";
  }
}

//This function is to get the Shared files data to a user:
function populateSharedFiles(userId) {
  let sharedFiles = [];
  $.ajax({
    type: "GET",
    url: "http://localhost:3000/uploads/fetchSharedToUser",
    data: { userId: userId },
    success: function (response) {
      sharedFiles = response;
    },
    async: false,
  });

  return sharedFiles;
}

//This function is to display data in the Shared Files Table:
function displaySharedFilesTable() {
  let loggedInUser = getLoginSession();
  let sharedFiles = populateSharedFiles(loggedInUser);

  for (file = 0; file < sharedFiles.length; file++) {
    var label = sharedFiles[file].label;
    var fileName = sharedFiles[file].fileName;
    var author = getUserInfo(sharedFiles[file].userId);
    if (!author) {
      continue;
    }
    const table = document
      .querySelector("#sharedUploadsTable")
      .getElementsByTagName("tbody")[0];
    var row = table.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var newLabel = document.createTextNode(label);
    var newFileName = document.createTextNode(
      sharedFiles[file].fileName + "_" + sharedFiles[file].parent
    );
    var newAuthor = document.createTextNode(author.fullName);

    cell1.appendChild(newLabel);
    cell2.appendChild(newFileName);
    cell2.className = "alignCenter";
    cell3.appendChild(newAuthor);
    cell3.className = "alignCenter";
  }
}

function shareFile(uploadId, selectedUser) {
  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/uploads/share`,
    data: { uploadId: uploadId, userId: selectedUser },
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

//This function is to delete a shared user:
function deleteSharedUser(uploadId, targetUser) {
  $.ajax({
    type: "PUT",
    url: `http://localhost:3000/uploads/unshare`,
    data: { uploadId: uploadId, userId: targetUser },
    success: function (response) {
      return true;
    },
    async: false,
    complete: function () {
      return true;
    },
  });
}

//This function is for the delete Shared Modal:
function deleteSharedUserModal(uploadId, targetUser) {
  var modal = document.querySelector("#deleteSharedUserModal");
  var span = document.querySelector("#closeDeleteSharedUser");
  var btnConfirm = document.querySelector("#btnDeleteSharedUser");
  var btnCancel = document.querySelector("#btnCancelDeleteSharedUser");
  modal.style.display = "block";

  btnConfirm.onclick = function () {
    deleteSharedUser(uploadId, targetUser);
    modal.style.display = "none";
    location.reload();
  };

  btnCancel.onclick = function () {
    modal.style.display = "none";
  };

  span.onclick = function () {
    modal.style.display = "none";
  };
}

/***  for Chats page: ***/

//This is to format the Date:
function formatDate(date) {
  let dayOfMonth = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();
  let hour = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  month = month < 10 ? "0" + month : month;
  dayOfMonth = dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth;
  hour = hour < 10 ? "0" + hour : hour;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return (
    year +
    "-" +
    month +
    "-" +
    dayOfMonth +
    " " +
    hour +
    ":" +
    minutes +
    ":" +
    seconds
  );
}

function sendMessage(message) {
  var userId = getLoginSession();
  var today = new Date();
  var chats = [];
  var chat = {
    chatId: Date.now(),
    sender: userId,
    message: message,
    date: formatDate(today),
  };

  $.ajax({
    type: "POST",
    url: "http://localhost:3000/chatlist",
    data: chat,
    success: function (response) {
      return false;
    },
  });
  return true;
}

function populateChats() {
  let chats;

  $.ajax({
    type: "GET",
    url: "http://localhost:3000/chatlist",
    success: function (response) {
      chats = response;
    },
    async: false, // <- this turns it into synchronous
  });
  return chats;
}

function removeChats() {
  var element = document.querySelector("#messages");
  element.innerHTML = "";
}

function displayAllChats() {
  let chats = populateChats();
  const messages = document.querySelector("#messages");
  console.log(chats);
  console.log(messages);

  if (chats.length === 0) {
    messages.innerHTML = `<p class='msg-el'> Send message to someone....</p>`;
  } else {
    removeChats();
    for (let i = 0; i < chats.length; i++) {
      var messageBody = chats[i].message;

      var dateSent = chats[i].date;

      var messageElement = document.createElement("p");
      var sender = chats[i].sender;
      console.log("sender", sender);

      if (getUserInfo(sender) == null) {
        messageElement.innerHTML +=
          "[" + dateSent + "] " + "<i>Inactive User</i>" + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      } else if (sender == getLoginSession()) {
        messageElement.innerHTML +=
          "[" + dateSent + "] " + "<b>You</b>" + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      } else {
        const name = getUserInfo(sender).fullName;
        messageElement.innerHTML +=
          "[" + dateSent + "] " + name + ": " + messageBody;

        messageElement.className = "msg-el";
        messages.appendChild(messageElement);
      }
    }
  }
  setTimeout(displayAllChats, 3000);
}

//This function is to display the name of logged in user on the Chat page:
function displayUsernameChatPage() {
  const loggedInUser = getUser();
  let userData;
  $.ajax({
    type: "GET",
    url: `http://localhost:3000/users/${loggedInUser._id}`,
    success: function (data) {
      console.log(data);
      if (data) {
        userData = data;
      }
    },
    async: false,
  });

  const spanName = document.getElementById("name");
  spanName.textContent = userData.fullName;
}

function addChatAjax() {
  const message = document.getElementById("message");
  const btnSend = document.getElementById("sendMessage");

  btnSend.onclick = function () {
    if (message.value !== "") {
      sendMessage(message.value);
      displayAllChats();
      message.value = "";
    } else {
      alert("Please add message");
    }
  };
}
